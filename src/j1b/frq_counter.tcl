# Scoped constraints for xpm_cdc_handshake
set reference_clk  [get_clocks -quiet -of [get_ports ref_clk]]
set meas_clk [get_clocks -quiet -of [get_ports frq_in]]

set reference_clk_period  [get_property -quiet -min PERIOD $reference_clk]
set meas_clk_period [get_property -quiet -min PERIOD $meas_clk]

#set xpm_cdc_hs_width [llength [get_cells dest_hsdata_ff_reg[*]]]
#set xpm_cdc_hs_num_s2d_dsync_ff [llength [get_cells xpm_cdc_single_src2dest_inst/syncstages_ff_reg[*]]]

if {$meas_clk == ""} {
    set slave_clk_period 1000
}

if {$reference_clk == ""} {
    set master_clk_period 1001
}

if {$meas_clk != $reference_clk} {
   set_false_path -to [get_cells gate_0*_reg*]
   set_false_path -to [get_cells gate_ack_sync_0*_reg*]
   set_max_delay -from $meas_clk -to [get_cells pulse_cnt*_reg*] $reference_clk_period -datapath_only
} elseif {$src_clk != "" && $dest_clk != ""} {
    common::send_msg_id "XPM_CDC_HANDSHAKE: TCL-1000" "WARNING" "The source and destination clocks are the same. \n     Instance: [current_instance .] \n  This will add unnecessary latency to the design. Please check the design for the following: \n 1) Manually instantiated XPM_CDC modules: Xilinx recommends that you remove these modules. \n 2) Xilinx IP that contains XPM_CDC modules: Verify the connections to the IP to determine whether you can safely ignore this message."
}
