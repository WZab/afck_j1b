-------------------------------------------------------------------------------
-- Title      : I2C controller driven by VIO objects
-- Project    : 
-------------------------------------------------------------------------------
-- File       : i2c_vio_ctrl_top.vhd
-- Author     : Wojciech M. Zabolotny wzab01<at>gmail.com
-- License    : PUBLIC DOMAIN
-- Company    : 
-- Created    : 2015-05-03
-- Last update: 2019-07-30
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-- This core allows you to configure different programmable clocks in the AFCK
-- board.
-- The core measures the frequency of clock on the FPGA_CLK1_P(N) differen-
-- tial input.
-- The I2C may control one of five busses, depending on the value written
-- to the Vio_i2c_sel
-- Suggested method of operation:
-- 1) 
-------------------------------------------------------------------------------
-- Copyright (c) 2015 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2015-05-03  1.0      wzab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;

library work;
use work.ipbus.all;
use work.wb_pkg.all;
use work.wishbone_wb_pkg.all;
use work.wishbone_pkg.all;
use work.agwb_main_wb_pkg.all;
library xpm;
use xpm.vcomponents.all;

entity i2c_j1b_ctrl_top is
  generic (
    NUM_I2CS : integer := 5);
  port (
    clk0_n           : in    std_logic;
    clk0_p           : in    std_logic;
    clk1_n           : in    std_logic;
    clk1_p           : in    std_logic;
    clk2_n           : in    std_logic;
    clk2_p           : in    std_logic;
    boot_clk         : in    std_logic;
    -- Pin needed to enable switch matrix
    clk_updaten      : out   std_logic;
    -- Pin needed to enable Si570
    si570_oe         : out   std_logic;
    -- Pins needed for Ethernet connection
    gt_clkp, gt_clkn : in    std_logic;
    gt_txp, gt_txn   : out   std_logic;
    gt_rxp, gt_rxn   : in    std_logic;
    --rst_p : in    std_logic;
    scl              : inout std_logic_vector(NUM_I2CS-1 downto 0);
    sda              : inout std_logic_vector(NUM_I2CS-1 downto 0)
--    uart_rxd         : out   std_logic;
--    uart_txd         : in    std_logic
    );

end entity i2c_j1b_ctrl_top;

architecture beh of i2c_j1b_ctrl_top is

  signal rst_n                      : std_logic := '0';
  signal rst_cnt                    : integer   := 1000000;
  signal rst125, rst125_0, rst125_1 : std_logic := '0';
  signal clk_200                    : std_logic := '0';

  constant c_gpi_num : integer := 4;
  constant c_gpo_num : integer := 4;

  signal gpi_i : std_logic_vector(c_gpi_num-1 downto 0);
  signal gpo_o : std_logic_vector(c_gpo_num-1 downto 0);

  signal frq0_in                        : std_logic;
  signal clk_frq0                       : std_logic_vector(31 downto 0);
  signal frq1_in                        : std_logic;
  signal clk_frq1                       : std_logic_vector(31 downto 0);
  signal frq2_in                        : std_logic;
  signal clk_frq2                       : std_logic_vector(31 downto 0);
  signal lpbck0, lpbck1, lpbck2, lpbck3 : std_logic_vector(31 downto 0);

  -- ipbus
  --signal mac_tx_data, mac_rx_data                                                                       : std_logic_vector(7 downto 0);
  --signal mac_tx_valid, mac_tx_last, mac_tx_error, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
  signal mac_addr                                                                                       : std_logic_vector(47 downto 0);
  signal clk125_out : std_logic;

-- WB signals as defined in wishbone_pkg
  signal wb_j1b_m2s : t_wishbone_master_out;
  signal wb_j1b_s2m : t_wishbone_master_in;
  signal wb_ipb_m2s : t_wishbone_master_out;
  signal wb_ipb_s2m : t_wishbone_master_in;
  signal wb_s125_m2s : t_wishbone_master_out;
  signal wb_s125_s2m : t_wishbone_master_in;
  signal wb_m125_m2s : t_wishbone_master_out;
  signal wb_m125_s2m : t_wishbone_master_in;
  signal wb_m_out   : t_wishbone_master_out_array(0 to 1);
  signal wb_m_in    : t_wishbone_master_in_array(0 to 1);
  signal wb_s_in    : t_wishbone_master_out_array(0 to 0);
  signal wb_s_out   : t_wishbone_master_in_array(0 to 0);

  -- WB signals defined in "lite" version
  signal wb_m2s, wb_slv_m2s : t_wb_m2s;
  signal wb_s2m, wb_slv_s2m : t_wb_s2m;

  signal wb_clk, wb_rst_0, wb_rst_1, wb_rst : std_logic := '1';

  constant c_address : t_wishbone_address_array(0 to 0) := (0 => std_logic_vector(to_unsigned(0, 32)));
  constant c_mask    : t_wishbone_address_array(0 to 0) := (0 => std_logic_vector(to_unsigned(0, 32)));

begin

  si570_oe    <= '1';
  clk_updaten <= '1';

  ibufgds0 : IBUFDS port map(
    i  => clk0_p,
    ib => clk0_n,
    --ceb => '0',
    o  => frq0_in
    );
  ibufgds1 : IBUFDS_GTE2 port map(
    i   => clk1_p,
    ib  => clk1_n,
    ceb => '0',
    o   => frq1_in
    );
  ibufgds2 : IBUFDS_GTE2 port map(
    i   => clk2_p,
    ib  => clk2_n,
    ceb => '0',
    o   => frq2_in
    );

  -- Frequency meters
  frq_counter_0 : entity work.frq_counter
    generic map (
      CNT_TIME   => 20000000,
      CNT_LENGTH => 32)
    port map (
      ref_clk => boot_clk,
      rst_p   => '0',
      frq_in  => frq0_in,
      frq_out => clk_frq0);

  frq_counter_1 : entity work.frq_counter
    generic map (
      CNT_TIME   => 20000000,
      CNT_LENGTH => 32)
    port map (
      ref_clk => boot_clk,
      rst_p   => '0',
      frq_in  => frq1_in,
      frq_out => clk_frq1);

  frq_counter_2 : entity work.frq_counter
    generic map (
      CNT_TIME   => 20000000,
      CNT_LENGTH => 32)
    port map (
      ref_clk => boot_clk,
      rst_p   => '0',
      frq_in  => frq2_in,
      frq_out => clk_frq2);


  process (boot_clk) is
  begin  -- process
    if boot_clk'event and boot_clk = '1' then  -- rising clock edge
      if rst_cnt > 0 then
        rst_n   <= '0';
        rst_cnt <= rst_cnt - 1;
      else
        rst_n <= '1';
      end if;
    end if;
  end process;

  clk_wiz_0_1 : entity work.clk_wiz_0
    port map (
      clk_out1 => clk_200,
      reset    => not rst_n,
      locked   => open,
      clk_in1  => boot_clk
      );

  -- J1B processor for I2C control
  j1_env_1 : entity work.j1_env
    generic map (
      clk_freq_g => 20e6,
      gpi_num_g  => c_gpi_num,
      gpo_num_g  => c_gpo_num)
    port map (
      clk      => boot_clk,
      rst_n    => rst_n,
      gpi_i    => gpi_i,
      gpo_o    => gpo_o,
--      uart_tx  => uart_rxd,
--      uart_rx  => uart_txd,
--      mdio     => PHY1_MDIO,
--      mdc      => PHY1_MDC,
      wb_m_out => wb_j1b_m2s,
      wb_m_in  => wb_j1b_s2m);

  wb_j1b_s2m  <= wb_m_in(0);
  wb_m_out(0) <= wb_j1b_m2s;

  wb_clk <= boot_clk;

  process (wb_clk, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      wb_rst_0 <= '1';
      wb_rst_1 <= '1';
      wb_rst   <= '1';
    elsif wb_clk'event and wb_clk = '1' then  -- rising clock edge
      wb_rst_0 <= '0';
      wb_rst_1 <= wb_rst_0;
      wb_rst   <= wb_rst_1;
    end if;
  end process;

  process (clk125_out, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      rst125_0 <= '1';
      rst125_1 <= '1';
      rst125   <= '1';
    elsif clk125_out'event and clk125_out = '1' then  -- rising clock edge
      rst125_0 <= '0';
      rst125_1 <= rst125_0;
      rst125   <= rst125_1;
    end if;
  end process;

  wb_cdc_1 : entity work.wb_cdc
    generic map (
      width => 32)
    port map (
      slave_clk_i    => clk125_out,
      slave_rst_n_i  => not rst125,
      slave_i        => wb_ipb_m2s,
      slave_o        => wb_ipb_s2m,
      master_clk_i   => wb_clk,
      master_rst_n_i => not wb_rst,
      master_i       => wb_m_in(1),
      master_o       => wb_m_out(1));

  wb_cdc_2 : entity work.wb_cdc
    generic map (
      width => 32)
    port map (
      slave_clk_i    => wb_clk,
      slave_rst_n_i  => not wb_rst,
      slave_i        => wb_s125_m2s,
      slave_o        => wb_s125_s2m,
      master_clk_i   => clk125_out,
      master_rst_n_i => not rst125,
      master_i       => wb_m125_s2m,
      master_o       => wb_m125_m2s);

  main_1 : entity work.main
    generic map (
      NUM_I2CS => NUM_I2CS)
    port map (
      rst_n_i   => not wb_rst,
      clk_sys_i => wb_clk,
      wb_s_in   => wb_m_out,
      wb_s_out  => wb_m_in,
      freq0     => clk_frq0,
      freq1     => clk_frq1,
      freq2     => clk_frq2,
      mac_ctrl_wb_m_o => wb_s125_m2s,
      mac_ctrl_wb_m_i => wb_s125_s2m,
      i2c_scl   => scl,
      i2c_sda   => sda);

  ipbus_top_1: entity work.ipbus_top
    port map (
      clk_200 => clk_200,
      clk125   => clk125_out,
      rst125   => rst125,
      rst_n    => rst_n,
      gt_clkp  => gt_clkp,
      gt_clkn  => gt_clkn,
      gt_rxp   => gt_rxp,
      gt_rxn   => gt_rxn,
      gt_txp   => gt_txp,
      gt_txn   => gt_txn,
      wb_m_m2s => wb_ipb_m2s,
      wb_m_s2m => wb_ipb_s2m,
      wb_s_m2s => wb_m125_m2s,
      wb_s_s2m => wb_m125_s2m);
  
  wb_s_out(0) <= wb2wishbone_s2m(wb_slv_s2m);
  wb_slv_m2s  <= wishbone2wb_m2s(wb_s_in(0));

end architecture beh;

