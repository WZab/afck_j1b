-- This file instantiates the Xilinx SGMII PCS/PMA block and conects it to the eth_usp_gmii
-- It is based on original eth_7s_1000basex written by Dave Newbold

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.VComponents.all;
use work.emac_hostbus_decl.all;
use work.agwb_main_wb_pkg.all;
use work.agwb_mac_ctrl_block_wb_pkg.all;

entity eth_usp_sgmii_wrapper is
  port(
    clk2             : in  std_logic;
    gt_clkp, gt_clkn : in  std_logic;
    gt_txp, gt_txn   : out std_logic;
    gt_rxp, gt_rxn   : in  std_logic;
    clk125_out       : out std_logic;
    rsti             : in  std_logic;
    locked           : out std_logic;
    tx_data          : in  std_logic_vector(7 downto 0);
    tx_valid         : in  std_logic;
    tx_last          : in  std_logic;
    tx_error         : in  std_logic;
    tx_ready         : out std_logic;
    rx_data          : out std_logic_vector(7 downto 0);
    rx_valid         : out std_logic;
    rx_last          : out std_logic;
    rx_error         : out std_logic;
    status_o         : out std_logic_vector(15 downto 0);
    pcs_ctrl         : in  t_pcs_ctrl;
    hostbus_in       : in  emac_hostbus_in := ('0', "00", "0000000000", X"00000000", '0', '0', '0');
    hostbus_out      : out emac_hostbus_out
    );

end eth_usp_sgmii_wrapper;

architecture rtl of eth_usp_sgmii_wrapper is

  component eth_usp_gmii is
    port (
      clk125      : in  std_logic;
      rst         : in  std_logic;
      --gmii_gtx_clk : out std_logic;
      gmii_txd    : out std_logic_vector(7 downto 0);
      gmii_tx_en  : out std_logic;
      gmii_tx_er  : out std_logic;
      gmii_rx_clk : in  std_logic;
      gmii_rxd    : in  std_logic_vector(7 downto 0);
      gmii_rx_dv  : in  std_logic;
      gmii_rx_er  : in  std_logic;
      tx_data     : in  std_logic_vector(7 downto 0);
      tx_valid    : in  std_logic;
      tx_last     : in  std_logic;
      tx_error    : in  std_logic;
      tx_ready    : out std_logic;
      rx_data     : out std_logic_vector(7 downto 0);
      rx_valid    : out std_logic;
      rx_last     : out std_logic;
      rx_error    : out std_logic;
      hostbus_in  : in  emac_hostbus_in := ('0', "00", "0000000000", X"00000000", '0', '0', '0');
      hostbus_out : out emac_hostbus_out);
  end component eth_7s_gmii;
  component gig_ethernet_pcs_pma_0
    port (
      gtrefclk_p             : in  std_logic;
      gtrefclk_n             : in  std_logic;
      gtrefclk_out           : out std_logic;
      gtrefclk_bufg_out      : out std_logic;
      txn                    : out std_logic;
      txp                    : out std_logic;
      rxn                    : in  std_logic;
      rxp                    : in  std_logic;
      independent_clock_bufg : in  std_logic;
      userclk_out            : out std_logic;
      userclk2_out           : out std_logic;
      rxuserclk_out          : out std_logic;
      rxuserclk2_out         : out std_logic;
      --gtpowergood            : out std_logic;
      resetdone              : out std_logic;
      pma_reset_out          : out std_logic;
      mmcm_locked_out        : out std_logic;
      sgmii_clk_r            : out std_logic;
      sgmii_clk_f            : out std_logic;
      sgmii_clk_en           : out std_logic;
      gmii_txd               : in  std_logic_vector(7 downto 0);
      gmii_tx_en             : in  std_logic;
      gmii_tx_er             : in  std_logic;
      gmii_rxd               : out std_logic_vector(7 downto 0);
      gmii_rx_dv             : out std_logic;
      gmii_rx_er             : out std_logic;
      gmii_isolate           : out std_logic;
      configuration_vector   : in  std_logic_vector(4 downto 0);
      an_interrupt           : out std_logic;
      basex_or_sgmii         : in  std_logic;
      an_adv_config_vector   : in  std_logic_vector(15 downto 0);
      an_restart_config      : in  std_logic;
      speed_is_10_100        : in  std_logic;
      speed_is_100           : in  std_logic;
      status_vector          : out std_logic_vector(15 downto 0);
      reset                  : in  std_logic;
      signal_detect          : in  std_logic;
      gt0_qplloutclk_out     : out std_logic;
      gt0_qplloutrefclk_out  : out std_logic
      );
  end component;

  component vio_0
    port (
      clk        : in  std_logic;
      probe_in0  : in  std_logic_vector(15 downto 0);
      probe_in1  : in  std_logic_vector(0 downto 0);
      probe_out0 : out std_logic_vector(0 downto 0)
      );
  end component;

  signal gmii_txd, gmii_rxd                                      : std_logic_vector(7 downto 0);
  signal gmii_tx_en, gmii_tx_er, gmii_rx_dv, gmii_rx_er          : std_logic;
  signal gmii_rx_clk                                             : std_logic;
  signal clkin, clk125, txoutclk_ub, txoutclk, clk125_ub, clk_fr : std_logic;
  signal clk62_5_ub, clk62_5, clkfb, rxoutclk_nb                 : std_logic;
  signal clk2_nbuf                                               : std_logic;

  signal phy_done, mmcm_locked : std_logic;
  signal status                : std_logic_vector(15 downto 0);

  signal gtrefclk_out  : std_logic;
  signal force_reset   : std_logic_vector(0 downto 0);
  signal v_mmcm_locked : std_logic_vector(0 downto 0);
  signal rsti2         : std_logic;

begin

  rsti2            <= rsti or force_reset(0) or pcs_ctrl.reset(0);
  v_mmcm_locked(0) <= mmcm_locked;

  --ibuf1 : IBUFDS_GTE2 port map(
  --  i   => clk2_p,
  --  ib  => clk2_n,
  --  o   => clk2_nbuf,
  --  ceb => '0'
  --  );

  --bufg2 : BUFG port map(
  --  i => clk2_nbuf,
  --  o => clk2
  --  );
  --bufg2 : BUFG port map(
  --  i => rxoutclk_nb,
  --  o => rxoutclk
  --);

--  clk125_fr <= clk_fr;

  locked <= mmcm_locked;

  eth_usp_gmii_1 : eth_usp_gmii
    port map (
      clk125      => clk125,
      rst         => rsti,
      --gmii_gtx_clk => open,
      gmii_txd    => gmii_txd,
      gmii_tx_en  => gmii_tx_en,
      gmii_tx_er  => gmii_tx_er,
      gmii_rx_clk => clk125,            --? Czy rxoutclk?
      gmii_rxd    => gmii_rxd,
      gmii_rx_dv  => gmii_rx_dv,
      gmii_rx_er  => gmii_rx_er,
      tx_data     => tx_data,
      tx_valid    => tx_valid,
      tx_last     => tx_last,
      tx_error    => tx_error,
      tx_ready    => tx_ready,
      rx_data     => rx_data,
      rx_valid    => rx_valid,
      rx_last     => rx_last,
      rx_error    => rx_error,
      hostbus_in  => hostbus_in,
      hostbus_out => hostbus_out);

  clk125_out <= clk125;
  status_o   <= status;

  --hostbus_out.hostrddata  <= (others => '0');
  --hostbus_out.hostmiimrdy <= '0';

  gig_ethernet_pcs_pma_0_2 : gig_ethernet_pcs_pma_0
    port map (
      gtrefclk_p             => gt_clkp,
      gtrefclk_n             => gt_clkn,
      gtrefclk_out           => gtrefclk_out,
      gtrefclk_bufg_out      => open,
      txn                    => gt_txn,
      txp                    => gt_txp,
      rxn                    => gt_rxn,
      rxp                    => gt_rxp,
      independent_clock_bufg => clk2,
      userclk_out            => open,
      userclk2_out           => clk125,
      rxuserclk_out          => open,
      rxuserclk2_out         => open,
      --gtpowergood            => open,
      resetdone              => phy_done,
      pma_reset_out          => open,
      mmcm_locked_out        => mmcm_locked,
      sgmii_clk_r            => open,
      sgmii_clk_f            => open,
      gmii_txd               => gmii_txd,
      gmii_tx_en             => gmii_tx_en,
      gmii_tx_er             => gmii_tx_er,
      gmii_rxd               => gmii_rxd,
      gmii_rx_dv             => gmii_rx_dv,
      gmii_rx_er             => gmii_rx_er,
      gmii_isolate           => open,
      configuration_vector   => pcs_ctrl.config_vec,   -- "10000" for AN 
      an_interrupt           => open,
      an_adv_config_vector   => pcs_ctrl.adv_cfg_vec,  -- IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      an_restart_config      => pcs_ctrl.an_restart_config(0),  --: IN STD_LOGIC;
      basex_or_sgmii         => pcs_ctrl.basex_or_sgmii(0),
      speed_is_10_100        => '0',
      speed_is_100           => '0',
      status_vector          => status,
      reset                  => rsti2,
      signal_detect          => '1',
      gt0_qplloutclk_out     => open,
      gt0_qplloutrefclk_out  => open
      );

  vio_0_1 : entity work.vio_0
    port map (
      clk        => clk125,
      probe_in0  => status,
      probe_in1  => v_mmcm_locked,
      probe_out0 => force_reset);

end rtl;

