-------------------------------------------------------------------------------
-- Title      : ipbus_top
-- Project    : 
-------------------------------------------------------------------------------
-- File       : ipbus_top.vhd
-- Author     : Wojciech M. Zabolotny  <wzab01@gmail.com>
-- Company    : 
-- Created    : 2019-07-30
-- Last update: 2019-07-30
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2019 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-07-30  1.0      WZab      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.ipbus.all;
use work.wishbone_pkg.all;
use work.agwb_main_wb_pkg.all;
use work.agwb_mac_ctrl_block_wb_pkg.all;


entity ipbus_top is

  port (
    clk_200 : in std_logic;
    clk125   : out std_logic;
    rst125   : in  std_logic;
    rst_n    : in  std_logic;
    gt_clkp  : in  std_logic;
    gt_clkn  : in  std_logic;
    gt_rxp   : in  std_logic;
    gt_rxn   : in  std_logic;
    gt_txp   : out std_logic;
    gt_txn   : out std_logic;
    -- WB master connections
    wb_m_m2s : out t_wishbone_master_out;
    wb_m_s2m : in  t_wishbone_master_in;
    -- WB slave conenctions
    wb_s_m2s : in  t_wishbone_slave_in;
    wb_s_s2m : out t_wishbone_slave_out
    );

end entity ipbus_top;

architecture rtl of ipbus_top is

  signal mac_tx_data, mac_rx_data : std_logic_vector(7 downto 0);
  signal mac_tx_valid, mac_tx_last, mac_tx_error,
    mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
  signal mac_addr : std_logic_vector(47 downto 0);
  signal mac_ls3b : std_logic_vector(31 downto 0);
  signal mac_ms3b : std_logic_vector(31 downto 0);

  signal ip_addr             : std_logic_vector(31 downto 0);
  signal status : std_logic_vector(15 downto 0);
  signal status_ext : std_logic_vector(31 downto 0);
  signal ipb_master_out                                                                                 : ipb_wbus;
  signal ipb_master_in                                                                                  : ipb_rbus;
  signal clk125_out : std_logic;

  signal pcs_ctrl : t_pcs_ctrl;


begin  -- architecture rtl
-- Conversion of IPbus signals to WB signals (sufficient fo classic single accesses)
  wb_m_m2s.dat <= ipb_master_out.ipb_wdata;
  wb_m_m2s.adr <= ipb_master_out.ipb_addr;
  wb_m_m2s.cyc <= ipb_master_out.ipb_strobe;
  wb_m_m2s.stb <= ipb_master_out.ipb_strobe;
  wb_m_m2s.sel <= (others => '1');
  wb_m_m2s.we  <= ipb_master_out.ipb_write;

  ipb_master_in.ipb_rdata <= wb_m_s2m.dat;
  ipb_master_in.ipb_ack   <= wb_m_s2m.ack;
  ipb_master_in.ipb_err   <= wb_m_s2m.err;

  clk125 <= clk125_out;

  ipbus : entity work.ipbus_ctrl
    port map(
      mac_clk      => clk125_out,
      rst_macclk   => rst125,
      ipb_clk      => clk125_out,
      rst_ipb      => rst125, -- not rst_n,
      mac_rx_data  => mac_rx_data,
      mac_rx_valid => mac_rx_valid,
      mac_rx_last  => mac_rx_last,
      mac_rx_error => mac_rx_error,
      mac_tx_data  => mac_tx_data,
      mac_tx_valid => mac_tx_valid,
      mac_tx_last  => mac_tx_last,
      mac_tx_error => mac_tx_error,
      mac_tx_ready => mac_tx_ready,
      ipb_out      => ipb_master_out,
      ipb_in       => ipb_master_in,
      mac_addr     => mac_addr,
      ip_addr      => ip_addr,
      pkt_rx       => open,
      pkt_tx       => open,
      pkt_rx_led   => open,
      pkt_tx_led   => open
      );


  eth_usp_sgmii_wrapper_1 : entity work.eth_usp_sgmii_wrapper
    port map (
      clk2        => clk_200,
      gt_clkp     => gt_clkp,
      gt_clkn     => gt_clkn,
      gt_txp      => gt_txp,
      gt_txn      => gt_txn,
      gt_rxp      => gt_rxp,
      gt_rxn      => gt_rxn,
      clk125_out  => clk125_out,
      rsti        => rst125, --not rst_n,
      locked      => open,
      tx_data     => mac_tx_data,
      tx_valid    => mac_tx_valid,
      tx_last     => mac_tx_last,
      tx_error    => mac_tx_error,
      tx_ready    => mac_tx_ready,
      rx_data     => mac_rx_data,
      rx_valid    => mac_rx_valid,
      rx_last     => mac_rx_last,
      rx_error    => mac_rx_error,
      status_o    => status,
      pcs_ctrl    => pcs_ctrl,
      hostbus_in  => open,
      hostbus_out => open);

  status_ext(31 downto 16) <= (others => '0');
  status_ext(15 downto 0)  <= status;

  agwb_mac_ctrl_block_wb_1 : entity work.agwb_mac_ctrl_block_wb
    port map (
      slave_i    => wb_s_m2s,
      slave_o    => wb_s_s2m,
      status_i   => status_ext,
      mac_ls3b_o => mac_ls3b,
      mac_ms3b_o => mac_ms3b,
      pcs_ctrl_o => pcs_ctrl,
      rst_n_i    => not rst125,
      clk_sys_i  => clk125_out);

  mac_addr <= mac_ms3b(23 downto 0) & mac_ls3b(23 downto 0);


end architecture rtl;
