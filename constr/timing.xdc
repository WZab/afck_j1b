create_clock -period 5.000 -name clk0_p -waveform {0.000 2.500} [get_ports clk0_p]
create_clock -period 5.000 -name clk1_p -waveform {0.000 2.500} [get_ports clk1_p]
create_clock -period 5.000 -name clk2_p -waveform {0.000 2.500} [get_ports clk2_p]

create_clock -period 50.000 -name boot_clk -waveform {0.000 25.000} [get_ports boot_clk]
create_clock -period 8.000 -name gt_clkp -waveform {0.000 4.000} [get_ports gt_clkp]
create_clock -period 33.333 -name j1_env_1/jtag_uart_1/jt_tck -waveform {0.000 16.667} [get_pins j1_env_1/jtag_uart_1/BSCANE2_1/TCK]

set_false_path -from [get_pins rst_n_reg/C] -to [get_pins rst125*_reg/PRE]

set_input_delay -clock [get_clocks boot_clk] -min -add_delay 0.000 [get_ports {scl[*]}]
set_input_delay -clock [get_clocks boot_clk] -max -add_delay 6.000 [get_ports {scl[*]}]
set_input_delay -clock [get_clocks boot_clk] -min -add_delay 0.000 [get_ports {sda[*]}]
set_input_delay -clock [get_clocks boot_clk] -max -add_delay 6.000 [get_ports {sda[*]}]
set_output_delay -clock [get_clocks boot_clk] -min -add_delay 0.000 [get_ports {scl[*]}]
set_output_delay -clock [get_clocks boot_clk] -max -add_delay 6.000 [get_ports {scl[*]}]
set_output_delay -clock [get_clocks boot_clk] -min -add_delay 0.000 [get_ports {sda[*]}]
set_output_delay -clock [get_clocks boot_clk] -max -add_delay 6.000 [get_ports {sda[*]}]

set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk125_out]
